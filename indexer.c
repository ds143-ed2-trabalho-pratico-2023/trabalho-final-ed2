#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>

#define FALSO 0
#define VERDADEIRO 1
#define COMPRIMENTO 100
#define NODOS 1000000

// Estrutura que guarda cada palavra de um arquivo e sua frequencia
struct NoPalavra
{
    char palavra[COMPRIMENTO];
    int frequencia;
} heap[NODOS];

// Estrutura que guarda o nome de um aquivo, a frequencia de um termo
// o total de palavras desse arquivo, e se indice tfidf
typedef struct
{
    char *nome;
    int frequencia_termo;
    int total_de_palavras;
    double tfidf;
} Documento;

int indice_nodo = 0;

// Declaração das funções
int frequencia_palavra_em_arquivo(FILE *arquivo, const char *palavra);
int total_palavras_em_arquivo(FILE *arquivo);
int heap_editar_no_existente(char *entrada);
void heap_adicionar_novo_no(char *entrada);
void heap_reorganizar_no_existente(int filho);
void heap_reorganizar(int raiz);
void heap_troca_nos(int a, int b);
int heap_esquerda(int raiz) { return (raiz * 2 + 1); }
int heap_direita(int raiz) { return (raiz * 2 + 2); }
int heap_pai(int filho) { return (filho - 1) / 2; }
int validar_palavra(char *entrada);
struct NoPalavra remover_max();
void quick_sort(Documento *documentos, int esquerda, int direita);

int main(int argc, char **argv)
{
    // OPÇÃO --FREQ
    if (strcmp(argv[1], "--freq") == 0)
    {
        FILE *arquivo = freopen(argv[3], "r", stdin);
        int n = atoi(argv[2]);

        // Adiciona cada paalvra do arquivo no HEAP
        char entrada[COMPRIMENTO] = {0};
        while (validar_palavra(entrada))
        {
            if (!strlen(entrada))
                continue;
            if (!heap_editar_no_existente(entrada))
                heap_adicionar_novo_no(entrada);
        }

        // Imprime os N elementos mais frequentes
        struct NoPalavra no_tmp;
        while (n--)
        {
            no_tmp = remover_max();
            printf("%s: %d\n", no_tmp.palavra, no_tmp.frequencia);
        }

        fclose(arquivo);
    }

    // OPÇÃO --FREQ-WORD
    else if (strcmp(argv[1], "--freq-word") == 0)
    {
        FILE *arquivo = freopen(argv[3], "r", stdin);
        char *palavraBuscada = argv[2];

        // Transforma palavra buscada para maiusculo
        for (int i = 0; palavraBuscada[i] != '\0'; i++)
        {
            palavraBuscada[i] = toupper(palavraBuscada[i]);
        }

        // Contagem da quantidade de palavras e saída
        int quantidade = frequencia_palavra_em_arquivo(arquivo, palavraBuscada);
        printf("A palavra %s aparece %i vezes no arquivo\n", palavraBuscada, quantidade);

        fclose(arquivo);
    }

    // OPÇÃO --SEARCH
    else if (strcmp(argv[1], "--search") == 0)
    {
        char *termo = argv[2];

        // Transforma termo para maiusculo
        for (int i = 0; termo[i] != '\0'; i++)
        {
            termo[i] = toupper(termo[i]);
        }

        int num_documentos = argc - 3;
        int num_documentos_termo_presente = 0;

        // Aloca memória para as estururas de documento com base
        // no número de documentos enviados
        Documento *documentos = (Documento *)malloc(num_documentos * sizeof(Documento));
        if (documentos == NULL)
        {
            perror("Erro ao alocar memória para documentos");
            exit(EXIT_FAILURE);
        }

        // Processa cada arquivo enviado
        for (int i = 0; i < num_documentos; i++)
        {
            documentos[i].nome = argv[i + 3];

            FILE *arquivo = freopen(argv[i + 3], "r", stdin);

            // Pesquisa frequencia de palavra no arquivo
            int quantidade = frequencia_palavra_em_arquivo(arquivo, termo);
            printf("ARQUIVO: %s\n", documentos[i].nome);
            printf("A palavra %s aparece %i vezes no arquivo\n", termo, quantidade);
            documentos[i].frequencia_termo = quantidade;

            // Acrescenta na variavel que conta o numero
            // de arquivos que tem o termo se tiver
            if (quantidade > 0)
            {
                num_documentos_termo_presente++;
            }

            // Pesquisa quantas palavrsa totais tem no arquivo
            int num_palavras = total_palavras_em_arquivo(arquivo);
            documentos[i].total_de_palavras = num_palavras;
            printf("A documento tem %d palavras\n\n", num_palavras);

            fclose(arquivo);
        }

        // Imprime os indices de relevancia para cada documento
        for (int i = 0; i < num_documentos; i++)
        {
            double tf = (double)documentos[i].frequencia_termo / documentos[i].total_de_palavras;
            printf("ARQUIVO: %s\n", documentos[i].nome);
            printf("tf %f \n", tf);

            double idf = log10(num_documentos / num_documentos_termo_presente);
            printf("idf %f \n", idf);

            double tfidf = tf * idf;
            printf("tfidf %f \n\n", tfidf);

            documentos[i].tfidf = tfidf;
        }

        // Ordenar os documentos por relevância: QUICK SORT
        quick_sort(documentos, 0, num_documentos - 1);

        // Exibir a lista ordenada
        printf("INDICES DE RELEVANCIA: \n");
        for (int i = 0; i < num_documentos; i++)
        {
            printf("%s: %lf\n", documentos[i].nome, documentos[i].tfidf);
        }

        free(documentos);
    }

    // LOG: tempo de execução
    float decorrido = (float)(clock()) / CLOCKS_PER_SEC;
    printf("Operacao realizada em: %f segundos\n", decorrido);

    return 0;
}

// Implementação das funções

void heap_troca_nos(int a, int b)
{
    int t;
    char t_c[COMPRIMENTO];

    t = heap[a].frequencia;
    heap[a].frequencia = heap[b].frequencia;
    heap[b].frequencia = t;

    strcpy(t_c, heap[a].palavra);
    strcpy(heap[a].palavra, heap[b].palavra);
    strcpy(heap[b].palavra, t_c);
}

void heap_adicionar_novo_no(char *entrada)
{
    heap[indice_nodo].frequencia = 1;
    strcpy(heap[indice_nodo].palavra, entrada);
    indice_nodo++;
}

int heap_editar_no_existente(char *entrada)
{
    int i;
    for (i = 0; i < indice_nodo; i++)
    {
        if (!strcmp(heap[i].palavra, entrada))
        {
            heap[i].frequencia++;
            heap_reorganizar_no_existente(i);
            return VERDADEIRO;
        }
    }
    return FALSO;
}

void heap_reorganizar_no_existente(int filho)
{
    int raiz = heap_pai(filho);
    if (heap[raiz].frequencia >= heap[filho].frequencia || raiz < 0)
        return;
    heap_troca_nos(raiz, filho);

    heap_reorganizar_no_existente(raiz);
}

void heap_reorganizar(int raiz)
{
    int filho = heap_esquerda(raiz);
    if (filho >= indice_nodo)
        return;
    if ((heap[filho].frequencia < heap[filho + 1].frequencia) &&
        (filho < (indice_nodo - 1)))
        filho++;
    if (heap[raiz].frequencia >= heap[filho].frequencia)
        return;
    heap_troca_nos(raiz, filho);
    heap_reorganizar(filho);
}

struct NoPalavra remover_max()
{
    indice_nodo--;
    if (indice_nodo != 0)
    {
        heap_troca_nos(0, indice_nodo);
        heap_reorganizar(0);
    }
    return heap[indice_nodo];
}

int validar_palavra(char *entrada)
{
    char c;
    int ind = 0;
    while (scanf("%c", &c) != EOF)
    {
        if (!(c >= '0' && c <= '9') && !((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')))
        {
            entrada[ind] = '\0';
            return VERDADEIRO;
        }
        entrada[ind++] = toupper(c);
    }
    return FALSO;
}

int frequencia_palavra_em_arquivo(FILE *arquivo, const char *palavra)
{
    char linha[COMPRIMENTO];
    int quantidade = 0;

    rewind(arquivo);

    while (fgets(linha, COMPRIMENTO, arquivo) != NULL)
    {
        char *palavraMaiuscula = strdup(palavra);
        char *palavraLida = strtok(linha, " \t\n");

        for (int i = 0; palavraMaiuscula[i]; i++)
        {
            palavraMaiuscula[i] = toupper(palavraMaiuscula[i]);
        }

        while (palavraLida != NULL)
        {
            char *palavraLidaMaiuscula = strdup(palavraLida);

            for (int i = 0; palavraLidaMaiuscula[i]; i++)
            {
                palavraLidaMaiuscula[i] = toupper(palavraLidaMaiuscula[i]);
            }

            if (strcmp(palavraLidaMaiuscula, palavraMaiuscula) == 0)
            {
                quantidade++;
            }

            free(palavraLidaMaiuscula);
            palavraLida = strtok(NULL, " \t\n");
        }

        free(palavraMaiuscula);
    }

    return quantidade;
}

int total_palavras_em_arquivo(FILE *arquivo)
{
    int count = 0;
    char word[100];

    rewind(arquivo);

    while (fscanf(arquivo, "%s", word) != EOF)
    {
        count++;
    }

    return count;
}

void quick_sort(Documento *documentos, int esquerda, int direita)
{
    int i = esquerda, j = direita;
    double pivo = documentos[(esquerda + direita) / 2].tfidf;

    while (i <= j)
    {
        while (documentos[i].tfidf > pivo)
        {
            i++;
        }

        while (documentos[j].tfidf < pivo)
        {
            j--;
        }

        if (i <= j)
        {
            Documento temp = documentos[i];
            documentos[i] = documentos[j];
            documentos[j] = temp;
            i++;
            j--;
        }
    }

    if (esquerda < j)
    {
        quick_sort(documentos, esquerda, j);
    }

    if (i < direita)
    {
        quick_sort(documentos, i, direita);
    }
}
