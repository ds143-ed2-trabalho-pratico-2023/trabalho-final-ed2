
# Indexer

Tecnologia em Análise e Desenvolvimento de Sistemas

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

DS143 - Estrutura de Dados 2

Prof. Alexander Robert Kutzke

## Alunas

Participaram do projeto as seguintes alunas:

- Lívia de Araújo Nunes
- Melissa Silva de Oliveira
- Natasha Alcaide Santos

## Sinopse

O **indexer** é um programa que realiza a contagem de palavras em documentos de texto. Ele oferece funcionalidades para identificar as N palavras mais frequentes em um documento, contar a ocorrência de uma palavra específica e buscar documentos relevantes para um termo específico.

## Instruções de Execução

1. **Clonar o Repositório:**
   ```bash
   git clone https://gitlab.com/ds143-ed2-trabalho-pratico-2023/trabalho-final-ed2.git
   cd trabalho-final-ed2
   ```

2. **Compilar o Arquivo `.c`:**
   ```bash
   gcc indexer.c -o indexer
   ```

3. **Executar o Programa:**
   ```bash
   ./indexer.exe + opções (ver "Descrição")
   ```

## Descrição

O programa **indexer** realiza a contagem de palavras em documentos de texto, permitindo a busca por ocorrências específicas ou a identificação das palavras mais frequentes. 

**IMPORTANTE:** Para executar o programa com seu texto de escolha, adicione-o na pasta clonada.

### Modo --freq

Ao ser executado com a opção `--freq`, o programa **indexer** exibe o número de ocorrências das N palavras mais frequentes no documento passado como parâmetro, apresentando os resultados em ordem decrescente de ocorrências.

#### Exemplo de Uso
```bash
indexer.exe --freq 10 arquivo.txt
```

### Modo --freq-word

Quando executado com a opção `--freq-word`, o programa **indexer** exibe a contagem de uma palavra específica no documento passado como parâmetro.

#### Exemplo de Uso
```bash
indexer.exe --freq-word palavra arquivo.txt
```

### Modo --search

Ao ser executado com a opção `--search`, o programa **indexer** apresenta uma listagem dos documentos mais relevantes para um dado termo de busca, utilizando o cálculo TF-IDF (Term Frequency-Inverse Document Frequency).

#### Exemplo de Uso
```bash
indexer.exe --search termo texto1.txt texto2.txt
```

## Opções

- `--freq N ARQUIVO`: Exibe o número de ocorrência das N palavras que mais aparecem em ARQUIVO, em ordem decrescente de ocorrência.
- `--freq-word PALAVRA ARQUIVO`: Exibe o número de ocorrências de PALAVRA em ARQUIVO.
- `--search TERMO ARQUIVO [ARQUIVO ...]`: Exibe uma listagem dos ARQUIVOS mais relevantes encontrados pela busca por TERMO, apresentando os resultados em ordem decrescente de relevância.